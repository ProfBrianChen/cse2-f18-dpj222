/////////////
//// CSE 02 
/// Daniel John
/// September 9, 2018
/// 
//// Compute the cost of the items you bought, including the PA sales tax of 6%.
//
public class Arithmetic {

public static void main(String args[]){
  
  //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtsPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltsPrice = 33.99;

//the tax rate
double paSalesTax = 0.06;

  double totalCostOfPants; // the total cost of pants
  double totalCostOfShirts; // the total cost of shirts
  double totalCostOfBelts; // the total cost of belts
  
  double taxCostOfPants; // the total amount taxed from the pants 
  double taxCostOfShirts; // the total amount taxed from the shirts
  double taxCostOfBelts; // the total amount taxed from the belts
  
  double totalCostOfPurchase; // the total cost of the purchase
  
  double totalSalesTax; // the total tax of the sale
  double totalTransaction; // the total cost of the transaction 
  
  totalCostOfPants = numPants * pantsPrice; // the total cost of pants outputted through multiplying number of pants with the price of the pants
  taxCostOfPants = ((int)(totalCostOfPants * paSalesTax * 100)) / 100.0; // the total cost of the taxes for the pants outputted by multiplying the total cost of pants with the PA sales tax
  
  totalCostOfShirts = numShirts * shirtsPrice; // the total cost of shirts outputted through multiplying number of shirts with the price of the shirts  
  taxCostOfShirts = ((int)(totalCostOfShirts * paSalesTax * 100)) / 100.0; // the total cost of the taxes for the shirts outputted by mutiplying the total cost of pants with the PA sales tax
  
  totalCostOfBelts = numBelts * beltsPrice; // the total cost of belts outputted through multiplying the number of belts with the price of the belts
  taxCostOfBelts = ((int)(totalCostOfBelts * paSalesTax * 100)) / 100.0; // the total cost of the taxes for the belts outputted by multiplying the total cost of belts with the PA sale tax
  
  totalCostOfPurchase = totalCostOfBelts + totalCostOfShirts + totalCostOfPants; // total cost of the purchase outputted by adding the total cost of the belts, shirts, and pants 
  totalSalesTax = ((int)(totalCostOfPurchase * paSalesTax * 100)) / 100.0; // total cost of the taxes for the entire purchase outputted by the total cost of the purchase with the PA sales tax
  
  totalTransaction = totalSalesTax + totalCostOfPurchase; // the total cost of the transaction outputted by the total sales tax added to the total cost of the purcahse
  
  System.out.println("Total cost of pants is " + totalCostOfPants + " dollars"); // prints out the total cost of pants in dollars 
  System.out.println("Total cost of shirts is " + totalCostOfShirts + " dollars"); // prints out the total cost of shirts in dollars 
  System.out.println("Total cost of belts is " + totalCostOfBelts + " dollars"); // prints out the total cost off the belts in dollars 
  System.out.println("Total tax of pants is " + taxCostOfPants + " dollars"); // prints out the total tax charged off the pants in dollars 
  System.out.println("Total tax of shirts is " + taxCostOfShirts + " dollars"); // prints out the total tax charged off the shirts in dollars 
  System.out.println("Total tax of belts is " + taxCostOfBelts + " dollars"); // prints out the total tax charged for the belts in dollars 
  System.out.println("Total cost of purchase is " + totalCostOfPurchase + " dollars"); // prints out the total cost of the purchase in dollars 
  System.out.println("Total sales tax is " + totalSalesTax + " dollars"); // print out the total tax charged off the entire purchase 
  System.out.println("Total transaction is " + totalTransaction + " dollars"); // prints out the total cost of the transaction 
  
  }
}

  
  