////////////
//// CSE 02 
/// Daniel John
/// December 3, 2018
/// 
/// create code that practices with multidimensional arrays   

import java.util.*; // import all the classes needed for the code 

public class HW10{ // make a name for the code that you creating 
 
 public static void main(String[] args) { // make a main method 
   
   char[ ][ ] charArray = new char[3][3]; // declare a new array 
    char numbers = '1'; // declare the variable numbers
   for(int i = 0; i < 3; i++){ // create a for loop for the mutidimensional array
     for(int j = 0; j < 3; j++){ // the second for loop for a 2D array
       charArray[i][j] = numbers; // create an array for the tic tac toe board
       numbers+=1; // add one to the variable numbers.

       
     } // close loop 
  
   } // close loop
   int counter = 2; // declare counter
   
  for(int i = 0; i < 3; i++){ //for loop for the multidimensional array
     for(int j = 0; j < 3; j++){ // second loop for the 2D array
       System.out.print(charArray[i][j] + " "); // print a space after every number one the board
       
     } // close loop
     System.out.println(); // print out the numbers on the board
     
 } // close loop
  
  Scanner myScanner = new Scanner(System.in); // declare a new class of scanner
   int choice = 0; // declare variable 
   boolean isWin = false; // boolean variable for when the player wins 
   char player = 'X'; // the first player 
  int [] mapCol = {0,1,2,0,1,2,0,1,2}; // the coordinates of the points on the baord 
  int [] mapRow = {0,0,0,1,1,1,2,2,2}; // the coordinates of the points on the board 
   while (!isWin){ // create a while loop for when the player does not win
     while (true){ // while loop for when the condition is true 
       choice =getInput(); // make choice equal to get input 
       choice = choice -1; // make choice equal to the choice of the user minus one
       if(!takenPlace(charArray, choice, mapCol, mapRow)){ // create an if statement for when the place the user picks is taken  already
       
        System.out.println("This place is taken"); // print a statement telling the user that the space is taken 
        continue; // continue on with the game 
       } // close if statement 
       else // create an else statement for the other condition
       {
          charArray[mapRow[choice]][mapCol[choice]] = player; // coordinate for the char array
          if(player == 'X') // if statement for when each player equals what latter
            player = 'O'; // if player one equals to O
          else // then
            player = 'X'; // player two equals to X
          isWin = testsWin(charArray); // declare who the winner is 
          printMethod(charArray); // print the array
       }
       break; // break out of the loop 
     }   
 }
   if ( counter % 2  == 0 ){ // create an if statement for when oen of the players win
            System.out.println("Player 1 won"); // tell player one that they won
        } else { // or else 
            System.out.println("Player 2 won"); // tell player two that they won
        }

}
 public static boolean takenPlace(char [][] charArray, int choice, int[] mapCol, int[] mapRow){ // create a method for when the place the user selects is taken  
 

    if(charArray[mapRow[choice]][mapCol[choice]] == 'X' || charArray[mapRow[choice]][mapCol[choice]] == 'O') //an if statement for when there is already an X or an O in the place that the user wants to put a letter at
      return false; // the player cannot do that
    else // or else
      return true; // if they dont select a spot that is already taken then they are good 
 }
 public static boolean testsWin(char [][] tests) // create a mothod that tests if any of the players won or not. 
 {
    for(int i =0; i < 3; i++) // make for loop 
    {
      if(tests[0][i] == tests[1][i] && tests[2][i] == tests[1][i]) // make an if statement for when the player gets 3 in a row in different rows 
      {
        return true; // player wins 
      }
      else if(tests[i][0] == tests[i][1] && tests[i][2] == tests[i][0]) // make an if statement for when the player gets 3 in a row in different columns 
        return true; // player wins 

    }

    if(tests[0][0] == tests[1][1] && tests[2][2] == tests[1][1]) // an if statement for diagonal three in a rows
    {
      return true; // player wins 
    }
    if(tests[0][2] == tests[1][1] && tests[2][0] == tests[1][1]) // an if statement for diagonal three in a rows 
    {
      return true; // player wins 
    }

    return false; // player does not win
   
 }
   
   
 
      
 
 public static void printMethod(char[][] input){ // create a method for the print 
   
   for(int i = 0; i < 3; i++){ // create a loop for multidimensional array
     for(int j = 0; j < 3; j++){ // another loop for a 2D array 
       System.out.print(input[i][j] + " "); // print a space after the numbers 
       
     } 
     System.out.println(); // print the numbers on the board
 }
 }
 
 public static int getInput(){ // create a method for asking the user to enter inputs
   Scanner myScanner = new Scanner(System.in); // create a new scanner 
   int input; // declare the input of the user 
     while (true){ // a while loop in the case that the number entered is not an integer 
       if (!myScanner.hasNextInt()){ // create an if statement for which the user enters something that is not an int
         
         System.out.println("Error. Please enter an integer: "); // print an error statement for when the user enters something that is not an int
         continue; // continue on after doing so 
       } // close if statement 
       
       else { // an else statement for all other cases  
         input = myScanner.nextInt(); // scan the input of the user 
       } // close else statement 
       
       if(input < 0 || input > 9){ // create an if statement for when the number entered is out of range 
         System.out.println("This is out of bounds. Enter another number: "); // error statement telling the user the input is out of bounds 
         continue; // continue 
     }
       break; // breake out of the loop 
     // System out print statement for the error in the case that the user does not enter an integer
   
   }
     return input; // save the value of the user
 }
}