////////////
//// CSE 02 
/// Daniel John
/// September 21, 2018
/// 
/// Create random generator of cards to practice for the big show at las vegas

import java.util.*;

public class CourseInformation {
 
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    int courseNum;
    String departName;
    int numTimesMeet;
    int timeClass;
    String instName;
    int numStudents;
    
    System.out.println("Enter the course number: ");
    while (!myScanner.hasNextInt()){
      myScanner.next();
      System.out.println("Error. Please enter the course number: ");
        
    } courseNum = myScanner.nextInt();
    
     System.out.println("Enter the departement name: ");
    while (!myScanner.hasNext()){
      myScanner.nextInt();
      System.out.println("Error. Please enter the departement name: ");
        
    } departName = myScanner.next();
    
    System.out.println("Enter the number if times you meet a week: ");
    while (!myScanner.hasNextInt()){
      myScanner.next();
      System.out.println("Error. Please enter the number if times you meet a week: ");
        
    } numTimesMeet = myScanner.nextInt();
    
    System.out.println("Enter the time the class starts: ");
    while (!myScanner.hasNextInt()){
      myScanner.next();
      System.out.println("Error. Please enter the time the class starts: ");
        
    } timeClass = myScanner.nextInt();
    
    System.out.println("Enter the instructor's name: ");
    while (!myScanner.hasNext()){
      myScanner.nextInt();
      System.out.println("Error. Please enter the instructor's name: ");
        
    } instName = myScanner.next();
    
    System.out.println("Enter the number of students in the class: ");
    while (!myScanner.hasNextInt()){
      myScanner.next();
      System.out.println("Error. Please enter the number of students in the class: ");
        
    } numStudents = myScanner.nextInt();
    
    System.out.println("Here is the course number: " + courseNum);
    System.out.println("Here is the departement name: " + departName);
    System.out.println("Here is the number of times you meet a week: " + numTimesMeet);
    System.out.println("Here is the time the class starts: " + timeClass);
    System.out.println("Here is the instructor's name: " + instName);
    System.out.println("Here is the number of students in the class: " + numStudents);
    
    
  }
}