////////////
//// CSE 02 
/// Daniel John
/// Novemeber 5, 2018
/// 
/// Create a code that shuffles cards and deals back a hand.    
///

import java.util.*; // import the classes  
public class hw08{ // name the code you are writing 


public static String[] getHand(String[] cards, int index, int numCards){ // create a method for the hand 
  
  String[] newArray = new String[numCards]; // create an array 
 
   for (int i = 0; i + index - numCards < index; i++){ // create a for loop 
     
     newArray[i] = cards[index - i]; // what the new array equals in terms of the original 
     
   }
   
   return newArray; // save the value for the new array 
  
}

public static void printArray(String[] list){ // create a new method
  
  System.out.print(Arrays.toString(list) + " "); // an out print statement for the stuff in the array 
  System.out.println("");// out print statement for the new line 
  
}

public static void shuffle(String[] cards){ // create a new method for the shuffling 
  
  for (int i = 0; i < 50; i++){ // create a loop 
    
    String tempVal = ""; // initialize the variable 
    int randomIndex = (int) (Math.random () * 51 + 1); // declare and get a random number for th eindex which is to be used 
    tempVal = cards[0]; // the index to be used 
    cards[0] = cards[randomIndex]; // pick a random index 
    cards[randomIndex] = tempVal; // make that random value equal to a dummy variable 
    
  }
}

public static String[] deckTwo(){ // create a new method for deck two of the cards 
  
  String[] suitNames = {"C","H","S","D"}; // decalre  and state the strings inside the array 
  String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; // declare and state the strings inside the variable 
  String[] cards = new String[52]; // decalre and state the amount of indexes in the array 
  for (int i = 0; i < 52; i++){ // create a for loop 
    
  cards[i] = rankNames[i % 13] + suitNames[i/13]; // what teh variable in the loop equals 
  System.out.print(cards[i] + " "); // outprint statement for the spaces 
  
  }
  
  return cards; // save the value for cards 

}

public static void main(String[] args) { // make a main method 
Scanner scan = new Scanner(System.in); // make a new scanner
  
  
String[] hand = new String[5]; // create an array
int numCards = 5; // declare the variable number of cards 
int again = 1; // declare a variable 
int index = 51; // declare a variable 
String[] cards = deckTwo(); // maek the array equal to a dummy variable 
System.out.println(""); // out print for the line spacing 
printArray(cards); // print the array of cards created 
System.out.println("Shuffled"); // outprint statement that shows that teh cards shown are shuffled
shuffle(cards); // shufffle the cards in the deck 
printArray(cards); // print the cards stored in the array 
System.out.println("Hands"); // outprint statement to let you know the cards displayed are the ones you have in  the hand 


while(again == 1){ // create a while loop for the case that again equals to 1
   hand = getHand(cards, index, numCards); // the variables in the hand 
   printArray(hand); // print the stuff in the array 
   System.out.println("Enter a 1 if you want another hand drawn"); // out print statement telling the user to enter 1 for another hand drawn 
   if(numCards > index){ // create an if statement 
   cards = deckTwo(); // make cards equal to the dummy variable 
   shuffle(cards); // shuffle the cards in the deck 
   index = 75; // how many times it will be shuffled 
   System.out.println("Deck two was started"); // out print statement to let the user know the start of deck two 

} 
   again = scan.nextInt(); // scan the next int for again

}  
  } 
}

