////////////
//// CSE 02 
/// Daniel John
/// October 8, 2018
/// 
/// Calculate the possibilities of combination of cards for a poker game. 

import java.util.*;

public class Hw05 {
 
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    int cardNumber = (int) ((Math.random() * 13) + 1);
    String cardNumberS = ""; 
   int cardSuitInt = (int) ((Math.random() * 4) + 1); 
    String cardSuit = "";
   int hands = (int) ((Math.random() * 5) + 1);
    String handsS = "";
    int userInput = 0;
    System.out.println("How many times should cards be generated: ");
    while (myScanner.hasNextInt()){
     userInput =  myScanner.nextInt();
     break;
     //System.out.println("Error. Please inter an integer for how many times cards should be generated: ");
      
    }
    double valueOne = 0;
     double valueTwo = 0;
     double valueThree = 0;
     double valueFour = 0; 
     double valueFive = 0;
   int randomNum = (int)((Math.random() * 52) + 1); 
   int cardOne;
   int cardTwo = (int)((Math.random() * 52) + 1);
   int cardThree = (int)((Math.random() * 52) + 1);
   int cardFour =  (int)((Math.random() * 52) + 1) ;
   int cardFive = (int)((Math.random() * 52) + 1);
   int handCount = 0;
   int onePairCount =0;
   int twoPairCount = 0;
   int threePairCount = 0;
   int fourPairCount = 0;
   
   for (int x = 0; x < userInput; x++){
     //System.out.println("terst");
     cardOne = (int)((Math.random() * 52) + 1);
     //case for 2
     while ( cardTwo == cardOne ){
         cardTwo = (int)((Math.random() * 52) + 1);
     }
     //case for 3
     while ( cardThree == cardOne || cardThree == cardTwo ){
         cardThree = (int)((Math.random() * 52) + 1);
     }
     //case for 4
     while ( cardFour == cardOne || cardFour == cardTwo || cardFour == cardThree ){
         cardFour = (int)((Math.random() * 52) + 1);
     }
     //case for 5
     while ( cardFive == cardOne || cardFive == cardTwo || cardFive == cardThree || cardFive == cardFour){
         cardFive = (int)((Math.random() * 52) + 1);
     }
     
     
     int pairCount = 0;
     
     System.out.println(cardOne);
     System.out.println(cardTwo);
     System.out.println(cardThree);
     System.out.println(cardFour);
     System.out.println(cardFive);
     
     
    
     if (cardOne % 13 == 0){
       valueOne = 13;
     }
     else{
     valueOne =  (cardOne / 13.0);
     
     if ( valueOne > 1 && valueOne < 2){
        valueOne = valueOne - 1;  
     }
     if (valueOne > 2 && valueOne < 3){
        valueOne = valueOne - 2;  
     }
     if (valueOne > 3 && valueOne < 4){
        valueOne = valueOne - 4;  
     }
     valueOne = (int)(valueOne * 13);
     }
   
      //card two
     
     if (cardTwo % 13 == 0){
       valueTwo = 13;
     }
     else{
     valueTwo =  (cardTwo / 13.0);
     
     if ( valueTwo > 1 && valueTwo < 2){
        valueTwo = valueTwo - 1;  
     }
     if (valueTwo > 2 && valueTwo < 3){
        valueTwo = valueTwo - 2;  
     }
     if (valueTwo > 3 && valueTwo < 4){
        valueTwo = valueTwo - 3;  
     }
     valueTwo = (int)(valueTwo * 13);
     }
  

      //card three
     
     if (cardThree % 13 == 0){
       valueThree = 13;
     }
     else{
     valueThree =  (cardThree / 13.0);
     
     if ( valueThree > 1 && valueThree < 2){
        valueThree = valueThree - 1;
     }
     if (valueThree > 2 && valueThree < 3){
        valueThree = valueThree - 2;  
     }
     if (valueThree > 3 && valueThree < 4){
        valueThree = valueThree - 3;
     }
     valueThree = (int)(valueThree * 13);
     
     }
     //card four
     
     if (cardFour % 13 == 0){
       valueFour = 13;
     }
     else{
     valueFour =  (cardFour / 13.0);
     
     
     if ( valueFour > 1 && valueFour < 2){
        valueFour = valueFour - 1;
     }
     if (valueFour > 2 && valueFour < 3){
        valueFour = valueFour - 2;  
     }
     if (valueFour > 3 && valueFour < 4){
        valueFour = valueFour - 3; 
     }
     valueFour = (int)(valueFour * 13);
     }
     //card five
     valueFive =  (cardFive / 13.0);
     if (valueFive % 13 == 0){
       valueFive = 13;
     }
     else{
     valueFive =  (cardFive / 13.0);
     
     
     if ( cardFive > 1 && valueFive < 2){
        valueFive = valueFive - 1;
     }
     if (valueFive > 2 && valueFive < 3){
        valueFive = valueFive - 2;  
     }
     if (valueFive > 3 && valueFive < 4){
        valueFive = valueFive - 3;
     }
     valueFive = (int)(valueFive * 13);
     }
     
     
     System.out.println("value one: " + valueOne);
     System.out.println("value two: " + valueTwo);
     System.out.println("value three: " + valueThree);
     System.out.println("value four: " + valueFour);
     System.out.println("value five: " + valueFive);
     
     
     
     
     
     if (valueOne == valueTwo){
       pairCount++;
     }
     if (valueOne == valueThree){
       pairCount++;
     }
     if (valueOne == valueFour){
       pairCount++;
     }
     if (valueOne == valueFive){
       pairCount++;
     }
       //end of counts for our first card 
     if (valueTwo == valueThree){
       pairCount++;
     }
     if (valueTwo == valueFour){
       pairCount++;
     }
     if (valueTwo == valueFive){
       pairCount++;
     }
       //now compare value two with value 3/4/5
     if (valueThree == valueFour){
       pairCount++;
     }
     if (valueThree == valueFive){
       pairCount++;
     }
     //
     if (valueFour == valueFive){
       pairCount++;
     }
    if (pairCount == 1){
        onePairCount++;
     }
     if (pairCount == 2){
        twoPairCount++;  
     }
     if (pairCount == 3){
        threePairCount++;
     }
     if (pairCount == 4){
       fourPairCount++;
     }
  } 
  
   
     
     
       System.out.println("The probability of One-pair: " + onePairCount / (double) userInput);
       System.out.println("The Probability of Two-pair: " + twoPairCount / (double) userInput);
       System.out.println("The Probability of Three-of-a-kind: " + threePairCount / (double) userInput);
       System.out.println("The Probability of Four-of-a-kind: " + fourPairCount / (double) userInput);
     
     
}
}