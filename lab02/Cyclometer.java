/////////////
//// CSE 02 
/// Daniel John
/// September 7, 2018
/// 
//// Measuring time elapsed in seconds, and the number of rotations of the front wheel during that time.
public class Cyclometer {
    	// start of main method
   	public static void main(String[] args) {


    int secsTrip1 = 480; // number of seconds traveled in trip 1
    int secsTrip2 = 3220; // number of seconds traveled in trip 2 
    int countsTrip1 = 1561; // number of roations in trip 1
    int countsTrip2 = 9037; // number of roations in trip 2 
//
//
    double wheelDiameter = 27.0; // diameter of wheel on the bicycle. 
    double PI = 3.14159; // ratio of the circumference of the diameter 
    int feetPerMile = 5280; // conversion of feet into miles
    int inchesPerFoot = 12; // conversion of inches into feet. 
    int secondsPerMinute = 60; // how many seconds there are in a minute
    double distanceTrip1; // ditance traveled in trip 1
    double distanceTrip2; // distance traveled in trip 2 
    double totalDistance; // total distance traveled in the two trips
//
//
    System.out.println("trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); // outputting number of minutes and count in trip 1
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); // outputting number of minutes and count in trip 2 
//
//
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // output distance in inches 
    distanceTrip1 /= inchesPerFoot * feetPerMile; // outputs distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // total distance traveled in trip 2
    totalDistance = distanceTrip1 + distanceTrip2;
//
//
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // outputting the distance traveled in trip 1 in miles
    System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // outputting the distance traveled in trip 2 in miles
    System.out.println("The total distance was " + totalDistance + " miles"); // outputting total distancetraveled in both trips. 
    }
}

