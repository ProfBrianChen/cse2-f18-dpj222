////////////
//// CSE 02 
/// Daniel John
/// November 9, 2018
/// 
/// Write a program that randomly places integers between 0-99 into an array and then counts the number of occurrences of each integer.

import java.util.*; 

public class Arrays{ 
 
  public static void main(String[] args) { 
    
    int[] numOfInts1 = new int[100];
    
    int[] numOfInts2 = new int[100]; 
    
    for(int i = 0; i < numOfInts1.length; i++){
     
      numOfInts1[i] = (int)(Math.random() * 100);
      
    
    if(numOfInts1[i] == numOfInts1[0]){
      
      System.out.print("Array 1 holds the following integers: ");
      
    }
    
    System.out.print(numOfInts1[i] + " ");
    
    } 
    
   
    for(int j = 0; j < numOfInts1.length; j++){
       
      int count = 0;
         
      for(int k = 0; k < numOfInts1.length; k++){
          
        if(numOfInts1[j] == numOfInts1[k]){
                        
          count++;
                    
        }    
                    
        numOfInts2[j] = count;
                
      }
                    
      if(numOfInts2[j] == 1){
        
        System.out.println(numOfInts1[j] + " occurs " + numOfInts2[j]+ " time");

      } else
      
      System.out.println(numOfInts1[j] + " occurs " + numOfInts2[j]+ " times");

      
             
    }

   }
  
  }