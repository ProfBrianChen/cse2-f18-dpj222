////////////
//// CSE 02 
/// Daniel John
/// Novemeber 5, 2018
/// 
/// Create a code that communicates with the user   

import java.util.*; 

public class Methods{ 
  
  public static void main(String[] args) { 
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter 1 to make a new sentence or enter 2 to exit: ");
    int userInput = myScanner.nextInt();
    
    String saveValue = thesisSentence();
    do
    {
      System.out.println("Enter 1 to make a new sentence or enter 2 to exit: "); 
            
          userInput = myScanner.nextInt(); 
    }while(userInput != 2);
    
      System.out.println();
      System.out.println();
      
     createParagraph();
      
      System.out.println(); 
    }
  
  public static String thesisSentence() {  
        String Subject = nonPrimarySubjectNouns();
        System.out.println("The" + " " + Adjectives() + " " + Adjectives() + " " + Subject + " " + pastTenseVerbs() + " the " + Adjectives() + " " + nonPrimarySubjectNouns() + ".");
         
     return Subject;
     
  }
  
  public static void actionSentence(String Subject) { 
        int random = (int)(Math.random() * 2) + 3; 
        if (random == 1) { 
            System.out.print("It used " + nonPrimaryObjectNouns() + " to " + pastTenseVerbs() + " " + nonPrimaryObjectNouns() + " at the " + Adjectives() + " " + nonPrimarySubjectNouns() + ".");
        } else {   
            System.out.print("This " + Subject + " was " + Adjectives() + " " + Adjectives() + " to " + pastTenseVerbs() + " " + nonPrimaryObjectNouns() + ".");
        }
    }
  
  public static void createParagraph() { 
        String Subject = thesisSentence(); 
        int random = (int)(Math.random() * 5);  
        for (int i = 0; i < random; i++) { 
            actionSentence(Subject);
        }
        conclusionSentence(Subject);

    }

    public static void conclusionSentence(String Subject) { //method to generate a conclusion sentence
        System.out.print("That " + Subject + " " + pastTenseVerbs() + " her " + nonPrimaryObjectNouns() + ".");


    }

  
  public static String Adjectives(){
    
    int random = (int)(Math.random() * 10);
    
    switch (random){
      case 1:
        return "beautiful";
      case 2:
        return "chubby";
      case 3:
        return "fancy";
      case 4:
        return "skinny";
      case 5:
        return "glamorous";
      case 6:
        return "little";
      case 7:
        return "gentle";
      case 8:
        return "silly";
      case 9:
        return "tiny";
      default:
        return "powerful";
    }
  }
  
  public static String nonPrimarySubjectNouns(){
    
    int random = (int)(Math.random() * 10);
    
    switch (random){
      case 1:
        return "child";
      case 2:
        return "dog";
      case 3:
        return "cat";
      case 4:
        return "man";
      case 5:
        return "woman";
      case 6:
        return "school";
      case 7:
        return "student";
      case 8:
        return "lion";
      case 9:
        return "tiger";
      default:
        return "teacher";
    }
  }
  
  public static String pastTenseVerbs(){
    
    int random = (int)(Math.random() * 10);
    
    switch (random){
      case 1:
        return "caught";
      case 2:
        return "bought";
      case 3:
        return "bit";
      case 4:
        return "blew";
      case 5:
        return "chose";
      case 6:
        return "came";
      case 7:
        return "burned";
      case 8:
        return "began";
      case 9:
        return "broke";
      default:
        return "took";
    }
  }
  
  public static String nonPrimaryObjectNouns(){
    
    int random = (int)(Math.random() * 10);
    
    switch (random){
      case 1:
        return "chair";
      case 2:
        return "shirt";
      case 3:
        return "pen";
      case 4:
        return "book";
      case 5:
        return "jacket";
      case 6:
        return "phone";
      case 7:
        return "building";
      case 8:
        return "car";
      case 9:
        return "train";
      default:
        return "computer";
    }
  }
}