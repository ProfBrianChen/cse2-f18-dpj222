////////////
//// CSE 02 
/// Daniel John
/// Novemebr 19, 2018
/// 
/// create code that practices arrays and single dimensional arrays  

import java.util.*; // import all the classes needed for the code 

public class RemoveElements{ // make a name for the code that you creating 
  
  public static void main(String[] args) { // make a main method
    
    Scanner myScan = new Scanner(System.in); // declare a new scanner 
    
    int num[]=new int[10]; // make a new array with a size of 10 
int newArray1[]; // create a new array 
int newArray2[]; // create a new array 
int index,target; // create a new variable 
 String answer=""; // create a new varible and decalre it as a string 
 do{ // create a do while loop
   System.out.print("Random input 10 ints [0-9]"); // print the random inout 
   num = randomInput(); // create a random number 
   String out = "The original array is:"; // set what the string out is equal to 
   out += listArray(num); // make the size of the array equal to the value of num
   System.out.println(out); // print out the string out 
 
   System.out.print("Enter the index "); // ask ther user to enter an index 
   index = myScan.nextInt(); // scan the input of the user 
   newArray1 = delete(num,index); // call in the delete method 
   String out1="The output array is "; // declare and make the string out equal to this statement 
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1); // print out the string out 1
 
      System.out.print("Enter the target value "); // ask the user to enter the target value 
   target = myScan.nextInt(); // scan the value that the user enters 
   newArray2 = remove(num,target); // call in the method remove 
   String out2="The output array is "; // declare and make a new string and make it equal to this statement 
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2); // print out the string out2
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-"); // ask the user if they want to go again or exit 
   answer=myScan.next(); // scan the input of the user 
 }while(answer.equals("Y") || answer.equals("y")); // create a while loop for when the user wants to go again 
  } // close the main method 
 
  public static String listArray(int num[]){ // create a new method for listArray 
 String out="{"; // declare a new string and make it equal to this statement 
 for(int j=0;j<num.length;j++){ // create a for loop for when j is less than the length of the numbers 
   if(j>0){ // create an if statement for when j is greater than 0
     out+=", "; // print this in that condition 
   } // close the if statement 
   out+=num[j]; // print out j in this condition 
 } // close the loop 
 out+="} "; // make a new string 
 return out; // save this value for out 
  } // close the method 



public static int[] randomInput(){ // create a new method for the random input of the user 
 int[] array = new int[10]; // create a new array 
 for(int i = 0; i < array.length; i++){ // create a for loop 
   array[i] = (int) (Math.random() * 10); // get a random value of 10 numbers for the array 
   
 } // close the for loop 
 
 return array; // save the value for array 
 
} // close the method 
    
public static int[] delete(int[] list, int num){ // make a new method for delete 
  int[] newArray = new int[list.length - 1]; // make a new array that is the size of the length of list minus 1
  int count = 0; // declare and create a new variable 
  int i = 0; // declare and create a new varaible 
  for(i = 0; i < list.length; i++){ // create a for loop for when the variable i is less than the length of the list 
    if(i == num){ // create an if statement for when i is equal to num
     continue; // continue 
    } // close the if statement 
    newArray[count] = list[i]; // make the size of the array equal to count 
    count++; // increment count 
  } // close the for loop 
  return newArray; // save the value for the new array 
  
} // close the method 

public static int[] remove(int[] list, int target){ // create a new method for remove 
  
  int counter = 0; // decalre and initialize counter 
 
  for(int i = 0; i < list.length; i++){ // create a for loop for when i is less than the length of list 
    
    if(list[i] == target){ // create an if statement for when the array list is equal to the value target 
      counter++; // increment counter 
    } // close the if statement 
  } // close the for loop 
  int[] newArray = new int[list.length - counter ]; // create a new array 
  int count = 0; // decalre and initialize count 
  for(int i = 0; i < list.length ; i++){ // create a for loop for when i is less than the length of list 
    if(list[i] != target){ // create an if statement for when the array list does not equal to the target value 
      newArray[count] = list[i]; // make the size of newarray equal to count 
      count++; // increment the value of count 
    } // close the if statement 
  } // close the for loop 
  return newArray; // save the value for newarray 
} // close the method 


  } // close the code 