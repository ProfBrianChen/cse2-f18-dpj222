////////////
//// CSE 02 
/// Daniel John
/// September 21, 2018
/// 
/// Randomly cast a dice and get the out put or allow user to evaluate dice themselves by picking two they they want to evaluate. 

import java.util.Scanner; // import scannner
import java.util.Random; // import random

public class CrapsIf { // Start of a class
 
  public static void main(String[] args) { // start of main method
    
    Scanner myScanner = new Scanner(System.in); // creating a new scanner type 
    System.out.println("Enter '1' to have dice randomly casted. Enter '2' to manually put two values yourself"); // out print statement asking user to pick which dice system they want to use
    int userInput = myScanner.nextInt(); // declare the input of the user
    
    if (userInput == 1){ // start of if statement if user inputs the first system as the one they want to use. 
      
      Random random = new Random(); // creating a new random type 
      int dice1 = random.nextInt(6)+1; // declare the first number as being an integer
      int dice2 = random.nextInt(6)+1; // decalre the second number as being an integer
      
      System.out.println("Here is dice 1: " + dice1); // outprint statement for the first dice
      System.out.println("Here is dice 2: " + dice2); // outprint statement for the second dice 
      
      if (dice1 == 1 && dice2 == 1){ // the two combination of dices for dice one and dice 2
        System.out.println("Snake Eyes"); // the outprint statement for the resulting terminology
          
      } 
      else if (dice1 == 1 && dice2 == 2){ // the two combination of dices for dice one and dice 2
        System.out.println("Ace Deuce"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 1 && dice2 == 3){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy Four"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 1 && dice2 == 4){ // the two combination of dices for dice one and dice 2
        System.out.println("Fever Five"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 1 && dice2 == 5){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy Six"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 1 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Seven out"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 2 && dice2 == 2){ // the two combination of dices for dice one and dice 2
        System.out.println("Hard four"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 2 && dice2 == 3){ // the two combination of dices for dice one and dice 2
        System.out.println("Fever five"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 2 && dice2 == 4){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy six"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 2 && dice2 == 5){ // the two combination of dices for dice one and dice 2
        System.out.println("Seven out"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 2 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy Eight"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 3 && dice2 == 3){ // the two combination of dices for dice one and dice 2
        System.out.println("Hard six"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 3 && dice2 == 4){ // the two combination of dices for dice one and dice 2
        System.out.println("Seven out"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 3 && dice2 == 5){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy Eight"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 3 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Nine"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 4 && dice2 == 4){ // the two combination of dices for dice one and dice 2
        System.out.println("Hard Eight"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 4 && dice2 == 5){ // the two combination of dices for dice one and dice 2
        System.out.println("Nine"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 4 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Easy Ten"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 5 && dice2 == 5){ // the two combination of dices for dice one and dice 2
        System.out.println("Hard Ten"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 5 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Yo-leven"); // the outprint statement for the resulting terminology
      }
      else if (dice1 == 6 && dice2 == 6){ // the two combination of dices for dice one and dice 2
        System.out.println("Boxcars"); // the outprint statement for the resulting terminology
      }
     
    }
    
          
    if (userInput == 2){ // if user picks the second option which is to put in the values themselves
      
      System.out.println("Enter dice number for dice 1: "); // outprint statement for the dice number users enter
      int numDice1 = myScanner.nextInt(); // declare dice 1 as an integer 
      System.out.println("Enter dice number for dice 2: "); // outprint statement for the number users enter the  
      int numDice2 = myScanner.nextInt(); // declare the dice second dice number as an integer 
   
        
   
      
      if (numDice1 == 1 && numDice2 == 1){ // combination of the two dice that users can enter 
        System.out.println("Snake Eyes"); // the outprint statement for the result of the combination of the dice number the user enters.
          
      } 
      else if (numDice1 == 1 && numDice2 == 2){ // combination of the two dice that users can enter
        System.out.println("Ace Deuce");
      }
      else if (numDice1 == 1 && numDice2 == 3){ // combination of the two dice that users can enter
        System.out.println("Easy Four");
      }
      else if (numDice1 == 1 && numDice2 == 4){ // combination of the two dice that users can enter
        System.out.println("Fever Five");
      }
      else if (numDice1 == 1 && numDice2 == 5){ // combination of the two dice that users can enter
        System.out.println("Easy Six");
      }
      else if (numDice1 == 1 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Seven out");
      }
      else if (numDice1 == 2 && numDice2 == 2){ // combination of the two dice that users can enter
        System.out.println("Hard four");
      }
      else if (numDice1 == 2 && numDice2 == 3){ // combination of the two dice that users can enter
        System.out.println("Fever five");
      }
      else if (numDice1 == 2 && numDice2 == 4){ // combination of the two dice that users can enter
        System.out.println("Easy six");
      }
      else if (numDice1 == 2 && numDice2 == 5){ // combination of the two dice that users can enter
        System.out.println("Seven out");
      }
      else if (numDice1 == 2 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Easy Eight");
      }
      else if (numDice1 == 3 && numDice2 == 3){ // combination of the two dice that users can enter
        System.out.println("Hard six");
      }
      else if (numDice1 == 3 && numDice2 == 4){ // combination of the two dice that users can enter
        System.out.println("Seven out");
      }
      else if (numDice1 == 3 && numDice2 == 5){ // combination of the two dice that users can enter
        System.out.println("Easy Eight");
      }
      else if (numDice1 == 3 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Nine");
      }
      else if (numDice1 == 4 && numDice2 == 4){ // combination of the two dice that users can enter
        System.out.println("Hard Eight");
      }
      else if (numDice1 == 4 && numDice2 == 5){ // combination of the two dice that users can enter
        System.out.println("Nine");
      }
      else if (numDice1 == 4 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Easy Ten");
      }
      else if (numDice1 == 5 && numDice2 == 5){ // combination of the two dice that users can enter
        System.out.println("Hard Ten");
      }
      else if (numDice1 == 5 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Yo-leven");
      }
      else if (numDice1 == 6 && numDice2 == 6){ // combination of the two dice that users can enter
        System.out.println("Boxcars");
      }
       
    }
    
  }
  
}
   