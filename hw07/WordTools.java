////////////
//// CSE 02 
/// Daniel John
/// October 27, 2018
/// 
/// Manipulate strings to force user to enter good input
import java.util.*; // import class

public class WordTools{ // state the class
  public static void main(String[] args) { // declaring the method
 
    Scanner myScanner = new Scanner(System.in); // declare new scanner
    String inputText = sampleText(); // declare variable
    String userInput = printMenu(); // declare variable

    
    switch(userInput)  { // create a switch statement for the different cases of inputs the user can enter
      case "c": // name the case
              int nonWs = getNumOfNonWSCharacters(inputText); // declare variable
              System.out.println("Number of non-Whitespace characters: " + nonWs); // STDOUT for the output of the program
      case "w": // name the case
              int wordCount = getNumOfWords(inputText); // declare the variable
              System.out.print("Number of words: " + wordCount); // STDOUT for the output of the program
               break; // break the leave
      case "f": // name the case
              System.out.println("Enter a word or phrase to be found: "); // STDOUT for the output of the program
              String phraseFound = myScanner.nextLine(); // scan the inputs the user enters
           
              System.out.println( phraseFound  + "instances: " + phraseFound(inputText, phraseFound)); // STDOUT for the output of the program
              //phraseFound(inputText);
              
               break; // break and leave
      case "r": // name the case
        String newText = replaceExclamation(userInput); // declare variable 
        
        System.out.println("Edited text: " + newText); // STOUT for the output of the code
              //replaceExclamation(inputText);
               break; // break out and leave
      case "s": // name the case
        String replacedText = shortenSpaces(userInput); // declare variable 
        System.out.println("Edited text: " + replacedText); // STDOUT for the output of the program
              //shortenSpace(inputText);
               break; // break out and leave 
      case "q": // name of the case 
      System.out.println("Exit program"); // STDOUT when exiting the program
      System.exit(0); // STDOUT of outcome of picking the output 
              //shortenSpace(inputText);
               break; // break and leave 
        
    }
  }
    
  
  
  
  
  
  
  
  
  
  
    public static String sampleText(){ // create a new method for a case 
    
      String userInput; // declare the variable 
      
      Scanner myScanner = new Scanner(System.in); // declare a new scanner 
      
      System.out.println("Enter a sample text: "); // STDOUT to ask the user to enter an input
      
      userInput = myScanner.nextLine(); // scan the input of the user 
    
      System.out.println("You entered: " + userInput); // STDOUT telling the user the text they entered
    
      return userInput; // save that value for the input of the user 
    }
    
    public static String printMenu(){ // make a new method for a case
      
      String userOption; // declare a variable 
     
      
        
        System.out.println("MENU"); // STDOUT displaying the menu for the user
        System.out.println("c - Number of non-whitespace characters"); // STDOUT for the first case
        System.out.println("w - Number of words"); // STDOUT for the second case 
        System.out.println("f - Find text"); // STDOUT for the third case 
        System.out.println("r - Repalce all !'s"); // STDOUT for the fourth case
        System.out.println("s - Shorten spaces"); // STDOUT for the fifth case 
        System.out.println("q - Quite"); // STDOUT for the sixth case
        System.out.println("Choose an option:"); // STDOUT telling the user to choose an option 
       Scanner myScanner = new Scanner(System.in); // declare a new scanner 
       
        userOption = myScanner.nextLine(); // scan the user input
        
        return userOption; // save the input of the user.
       
     
      }
      
    public static int getNumOfNonWSCharacters(String input){ // create a new method for the first case of the user input
      int count = 0; // decalre and initialize vatriable 
      for (int x = 0; x < input.length(); x++){ // create a loop for the case
        if (input.charAt(x) == ' '){ // create an if statement for the input of the user 
          count++; // increment 
          //input.charAt(x) =.
      }
    }
      return input.length() - count; // save the value 
    }
    
    
    public static int getNumOfWords(String input){ // create a new method for the new case that the user picks as an option 
      int wordCount = 0; // decalre variable 
      for (int x = 0; x < input.length(); x++){ // create a loop for that case 
        if (input.charAt(x) != ' '){ // create an if statement for the input of the user 
          if (input.charAt(x+1) == ' '){ // create an if statement for the input
            wordCount++; // increment the value 
          }
          
      }
        
    }
         return wordCount; // save the value for the variable 
    }
    
   
    

    
    
    public static int phraseFound(String input, String word){ // create a new method for the new case the user enters
      
      int testLength = word.length(); // declare the variable 
      int count = 0; // declare another variable 
      for (int x = 0; x < input.length(); x++){ // create a loop for the new input of the user 
        String current = input.substring(x, x + testLength); // declare a new variable 
          if (current.equals(word)) { // create an if statement for the current number 
              count++; // increment the variable 
            }
        
             if ( x + testLength == input.length()){ // create an if statement for the variable 
                break; // break and leave the loop 
             }
        
        
       }

      return count; // save the value for the variable 
   }
    
    public static String replaceExclamation(String input){ // create a new method for the new type of choice the user selects 
      
      String change = input.replaceAll("!", "."); // declare a variable 
      return change; // save value 
    }
    
    
    public static String shortenSpaces(String input){ // create a new method for the new type of input the user selects 
      
      String newWord = " "; // declare and initialize a variable 
      
      char x = 0; // declare a variable 
      for (x = 0; x < input.length(); x++) // create a for loop for the input 
        
        if (input.charAt(x) == ' ' && input.charAt(x + 1) == ' '); // create an if variable for the input of the user 
      newWord = input.substring(0, x) + " " + input.substring(x + 1, input.length()); // variable 
    return newWord; // save the value 
    }
    
    }