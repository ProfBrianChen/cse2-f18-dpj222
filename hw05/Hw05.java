////////////
//// CSE 02 
/// Daniel John
/// October 8, 2018
/// 
/// Calculate the possibilities of combination of cards for a poker game. 

import java.util.*; // import java

public class Hw05 { // import class
 
  public static void main(String[] args) { // start of code
    
    Scanner myScanner = new Scanner(System.in); // decalre new scanner type
    
    int cardNumber = (int) ((Math.random() * 13) + 1); // declare card number as integer
    String cardNumberS = ""; // decalre card number as string
   int cardSuitInt = (int) ((Math.random() * 4) + 1); // declare cardsuit as string 
    String cardSuit = ""; // initialize
   int hands = (int) ((Math.random() * 5) + 1); // declare hands as integer
    String handsS = ""; // declare hand as string 
    int userInput = 0; // declare user input as integer
    System.out.println("How many times should cards be generated: "); // write a STDOUT for number of times cards generated 
    while (myScanner.hasNextInt()){ // start a while statement 
     userInput =  myScanner.nextInt(); // declare new scanner type
     break; // break to exist loop
     //System.out.println("Error. Please inter an integer for how many times cards should be generated: ");
      
    }
    double valueOne = 0; // declare value as double
     double valueTwo = 0; // declare value as double
     double valueThree = 0; // declare value as double
     double valueFour = 0; // declare value as double
     double valueFive = 0; // declare value as double
   int randomNum = (int)((Math.random() * 52) + 1); // generate random number from 1 to 52
   int cardOne; // declare card as integer
   int cardTwo = (int)((Math.random() * 52) + 1); // declare card as integer
   int cardThree = (int)((Math.random() * 52) + 1); // declare card as integer
   int cardFour =  (int)((Math.random() * 52) + 1); // declare card as integer
   int cardFive = (int)((Math.random() * 52) + 1); // declare card as integer
   int handCount = 0; // declare handcount and set it equal to zero
   int onePairCount =0; // declare pair count and set equal to zero
   int twoPairCount = 0; // declare pair count and set equal to zero
   int threePairCount = 0; // declare pair count and set equal to zero
   int fourPairCount = 0; // declare pair count and set equal to zero
   
   for (int x = 0; x < userInput; x++){ // start a for loop about user input 
     //System.out.println("terst");
     cardOne = (int)((Math.random() * 52) + 1); // generate random of card one
     //case for 2
     while ( cardTwo == cardOne ){ // start a while loop for comparison 
         cardTwo = (int)((Math.random() * 52) + 1); // generate random value for card two 
     }
     //case for 3
     while ( cardThree == cardOne || cardThree == cardTwo ){ // start a while loop for comparison
         cardThree = (int)((Math.random() * 52) + 1); // generate random value for card three
     }
     //case for 4
     while ( cardFour == cardOne || cardFour == cardTwo || cardFour == cardThree ){ // start a while loop for comparison
         cardFour = (int)((Math.random() * 52) + 1); // generate random value for card four
     }
     //case for 5
     while ( cardFive == cardOne || cardFive == cardTwo || cardFive == cardThree || cardFive == cardFour){ // start a while loop for comparison
         cardFive = (int)((Math.random() * 52) + 1); // generate random value for card five 
     }
     
     
     int pairCount = 0; // declare pair count 
     
     System.out.println(cardOne); // write a print statement for card 
     System.out.println(cardTwo); // write a print statement for card
     System.out.println(cardThree); // write a print statement for card
     System.out.println(cardFour); // write a print statement for card
     System.out.println(cardFive); // write a print statement for card
     
     
    
     if (cardOne % 13 == 0){ // start an if statement for card one 
       valueOne = 13; // set value of card 1 equal to 13 
     }
     else{ // start an else statement for a different case 
     valueOne =  (cardOne / 13.0); // devide card one by 13 to get value one
     
     if ( valueOne > 1 && valueOne < 2){ // an if statement for each other case
        valueOne = valueOne - 1; // what the value of one equals to 
     }
     if (valueOne > 2 && valueOne < 3){ // an if statement for each other case
        valueOne = valueOne - 2; // what the value of one equals to
     }
     if (valueOne > 3 && valueOne < 4){ // an if statement for each other case
        valueOne = valueOne - 3; // what the value of one equals to
     }
     valueOne = (int)(valueOne * 13); // an if statement for each other case
     }
   
      //card two
     
     if (cardTwo % 13 == 0){ // start an if statement for card two 
       valueTwo = 13; // set value of two equal to 13
     }
     else{ // write an else statements for other cases 
     valueTwo =  (cardTwo / 13.0); // how to find the value of two
     
     if ( valueTwo > 1 && valueTwo < 2){ // if statement for value two 
        valueTwo = valueTwo - 1; // how to find value two   
     }
     if (valueTwo > 2 && valueTwo < 3){ // if statement for value two
        valueTwo = valueTwo - 2; // how to find value two 
     }
     if (valueTwo > 3 && valueTwo < 4){ // if statement for value two
        valueTwo = valueTwo - 3; // how to find value two 
     }
     valueTwo = (int)(valueTwo * 13); // how to find value two 
     }
  

      //card three
     
     if (cardThree % 13 == 0){ // if statement for card three
       valueThree = 13; // set card three equal to 13
     }
     else{ // else statement for all other cases
     valueThree =  (cardThree / 13.0); // how to find value three
     
     if ( valueThree > 1 && valueThree < 2){ // if statement for value three
        valueThree = valueThree - 1; // how to find value three 
     }
     if (valueThree > 2 && valueThree < 3){ // if statement for value three
        valueThree = valueThree - 2; // how to find value three
     }
     if (valueThree > 3 && valueThree < 4){ // if statement for value three
        valueThree = valueThree - 3; // how to find value three
     }
     valueThree = (int)(valueThree * 13); // how to find value three
     
     }
     //card four
     
     if (cardFour % 13 == 0){ // if statement for card four 
       valueFour = 13; // set value of four equal to 13
     }
     else{ // write an else statements for all other cases
     valueFour =  (cardFour / 13.0); // how to find value of card four 
     
     
     if ( valueFour > 1 && valueFour < 2){ // if statement for value of four 
        valueFour = valueFour - 1; // how to find value four 
     }
     if (valueFour > 2 && valueFour < 3){ // if statement for value of four
        valueFour = valueFour - 2; // how to find value four 
     }
     if (valueFour > 3 && valueFour < 4){ // if statement for value of four
        valueFour = valueFour - 3; // how to find value four 
     }
     valueFour = (int)(valueFour * 13); // how to find value four 
     }
     //card five
     
     if (cardFive % 13 == 0){ // if statement for card five 
       valueFive = 13; // set the value of five equal to 13
     }
     else{ // write an else statement for all other cases 
     valueFive =  (cardFive / 13.0); // how to find value five 
     
     
     if ( valueFive > 1 && valueFive < 2){ // if statement for value five 
        valueFive = valueFive - 1; // how to find value five 
     }
     if (valueFive > 2 && valueFive < 3){ // if statement for value five
        valueFive = valueFive - 2; // how to find value five 
     }
     if (valueFive > 3 && valueFive < 4){ // if statement for value five
        valueFive = valueFive - 3; // how to find value five 
     }
     valueFive = (int)(valueFive * 13); // how to find value five 
     }
     
     
     System.out.println("value one: " + valueOne); // STDOUT for value 
     System.out.println("value two: " + valueTwo); // STDOUT for value
     System.out.println("value three: " + valueThree); // STDOUT for value
     System.out.println("value four: " + valueFour); // STDOUT for value
     System.out.println("value five: " + valueFive); // STDOUT for value
     
     
     
     
     
     if (valueOne == valueTwo){ // if statement for one case  
       pairCount++; // increment if true 
     }
     if (valueOne == valueThree){ // if statement for one case 
       pairCount++; // increment if true
     }
     if (valueOne == valueFour){ // if statement for one case
       pairCount++; // increment if true
     }
     if (valueOne == valueFive){ // if statement for one case
       pairCount++; // increment if true
     }
       //end of counts for our first card 
     if (valueTwo == valueThree){ // if statement for one case 
       pairCount++; // increment if true
     }
     if (valueTwo == valueFour){ // if statement for one case
       pairCount++; // increment if true
     }
     if (valueTwo == valueFive){ // if statement for one case
       pairCount++; // increment if true
     }
       //now compare value two with value 3/4/5
     if (valueThree == valueFour){ // if statement for one case
       pairCount++; // increment if true
     }
     if (valueThree == valueFive){ // if statement for one case
       pairCount++; // increment if true
     }
     //
     if (valueFour == valueFive){ // if statement for one case
       pairCount++; // increment if true
     }
    if (pairCount == 1){ // if statement for pair count as it equals a value 
        onePairCount++; // increment if true
     }
     if (pairCount == 2){ // if statement for pair count as it equals a value
        twoPairCount++; // increment if true 
     }
     if (pairCount == 3){ // if statement for pair count as it equals a value
        threePairCount++; // increment if true
     }
     if (pairCount == 4){ // if statement for pair count as it equals a value
       fourPairCount++; // increment if true
     }
  } 
  
   
     
     
       System.out.println("The probability of One-pair: " + onePairCount / (double) userInput); // STDOUT for probability of one pair 
       System.out.println("The Probability of Two-pair: " + twoPairCount / (double) userInput); // STDOUT for probability of two pair 
       System.out.println("The Probability of Three-of-a-kind: " + threePairCount / (double) userInput); // STDOUT for probability of three of a kind 
       System.out.println("The Probability of Four-of-a-kind: " + fourPairCount / (double) userInput); // STDOUT for probability of four of a kind 
     
     
}
}
