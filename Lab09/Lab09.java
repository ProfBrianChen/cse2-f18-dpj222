////////////
//// CSE 02 
/// Daniel John
/// Novemeber 16, 2018
/// 
/// Will illustrate the effects of passing arrays as method arguments  
///

import java.util.*;   
public class Lab09{ 
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    int[] input = {1,2,3,4,5,6,7,8,9,10};
    int[] input1 = copy (input);
    int[] input2 = copy (input);
    
    inverter(input);
      
      print(input);
      
      System.out.println();
      inverter2(input1);
      
    print(input1);
    
    System.out.println();
    
    print(input2);
    
    System.out.println();
    int[] input3 = inverter2(input2);
    
    print(input3);
    
  }
  
  public static int[] copy(int[] userInput){ 
    
    int [] input2 = new int[userInput.length];
    for(int i = 0; i < userInput.length; i++){
      
      input2[i] = userInput[i];
      
    }
    
    return input2;
  }
  
  
  public static void inverter(int[] input){ 
    
    for(int i = 0; i < input.length / 2; i++){
      
      int tempInput = input[i];
      input[i] = input[(input.length - 1) - i];
      input[(input.length - 1) - i] = tempInput;
      
    }
    
  }
    
     public static int[] inverter2(int[] input){ 
    
     int [] newInput = copy (input);
     for(int i = 0; i < newInput.length / 2; i++){
      
      int tempInput = input[i];
      newInput[i] = newInput[(newInput.length - 1) - i];
      newInput[(newInput.length - 1) - i] = tempInput;
    }
     return newInput;
   }
    
   public static void print(int[] input){ 
        
     for (int i = 0; i < input.length; i++){ 
            
       System.out.print(input[i] + " "); 
        
     } 
        
        System.out.println(); 
 
    } 

    
  }
