////////////
//// CSE 02 
/// Daniel John
/// October 12, 2018
/// 
/// Create nested loops that display a set of pattern of numbers  

import java.util.*; 

public class PatternA { 
 
  public static void main(String[] args) { 
    
    Scanner myScanner = new Scanner(System.in);
     int numRows = 0;
      
    System.out.println("Please enter an integer between 1-10: ");
    while (!myScanner.hasNextInt()){
      myScanner.next();
      System.out.println("Error. Please enter an integer: ");
    }
    numRows = myScanner.nextInt();
    
    while (numRows < 1 || numRows > 10){
      System.out.println("Error. Please input an integer between 1 and 10 only: ");
      numRows = myScanner.nextInt();
          }
    for (int i = 1; i <= numRows; i++){
      for (int j = 1; j <= i; j++){
      System.out.print(j);  
    
    }
      System.out.println();
  }
}
}
    
      
      