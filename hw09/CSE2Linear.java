////////////
//// CSE 02 
/// Daniel John
/// Novemebr 19, 2018
/// 
/// create code that practices arrays and single dimensional arrays  

import java.util.*; // import all the classes needed for the code 

public class CSE2Linear{ // make a name for the code that you creating 
  
  public static void main(String[] args) { // make a main method 
    
    Scanner myScanner = new Scanner(System.in); // declare a new scanner 
    int userInput; // declare a new variable 
    int[] studentGrades = new int[15]; // create a new array 
    
    
    System.out.println("Please enter 15 integers between 0-100 from least to greatest: "); // print a statement that asks the user to enter 15 ints between 0 and 100
    int i = 0; // declare the variable i and initialize it
    while (i < 15){ // create a while loop to check if what the user entered is what is asked for 
      
    
    
    if (!myScanner.hasNextInt()){ // create an if statement for which the user enters something that is not an int
       
      System.out.println("Error. Please enter an integer: "); // print an error statement for when the user enters something that is not an int
      continue; // continue on after doing so 
    } // close if statement 
    
    else { // an else statement for all other cases  
      userInput = myScanner.nextInt(); // scan the input of the user 
    } // close else statement 
    
    if (userInput < 1 || userInput > 100){ // an else statement for when the user enters something out of range
      System.out.println("Error. Please enter 15 integers between 1 and 100 only: "); // print an error statement for when the numbers entered are out of range 
      continue; // continue on with the code 
    } // close the if statement 
    
    
    if (i > 0 && userInput <  studentGrades[i - 1]){ // create an if stastement for when the numbers entered ar enot from least to greatest 
      System.out.println("Error. Please enter 15 integers between 1 and 100 from least to greatest: "); // print the error statement for this case 
      continue; // continue on with the code 
    } // close the if statement 
    
   studentGrades[i] = userInput; // make the array of the students grades equal to the input of the user 
   i++; // increment the value of i 
  
    } // close the while loop 
    
    for (int x = 0; x < studentGrades.length; x++){ // close the for loop 
      System.out.print(studentGrades[x] + " "); // print out the array of grades of the student with spaces after them 
    }
    
    System.out.println("Enter a grade to be searched for: "); // ask the user to enter what grade they want to be searched 
    userInput = myScanner.nextInt(); // scan the input of the user
    binarySearch(studentGrades, userInput); // call in the binary search method used to searhc for the grade 
    shuffle(studentGrades); // shuffle the grades that the user entered 
    System.out.println("Enter a grade to be searched for: "); // ask the user to enter a grade to be searched for 
    userInput = myScanner.nextInt(); // scan the input of the user 
    linearSearch(studentGrades, userInput); // call the linear search method used to search for the the grade the user wants found  
    
  } // close the for loop 
  
  public static void binarySearch(int[] input, int number) { // create a new method for the binary search for the grade that the user entered 
        int first = 0; // declare a variable for the first number 
        int last = input.length - 1; // declare the make the last number the length of the array of the user input 
        int middle = (first + last) / 2; // decalre the variable for the middle number and how to find it 
        int iterationCount = 0; // declare a variable for the number of iterations count to find the number and initialize it 
        while (first <= last) { // create a while for when the first number is less than or equal to the last number 
            iterationCount++; // increment the itteration count 
            if (input[middle] < number) { // create an if statement for when the value in the middle of the array is less than the number 
                first = middle + 1; // in this condition make the first equal to the middle plus one 
            } else if (input[middle] == number) { // create an else if statement for when the value in the middle is equal to the number 
                System.out.println(number + " was found with " + iterationCount + " iterations"); // print out how many iterations it took to find the number 
                return; // return the value 

            } else { // create an else statement for the conditions outside of the ones listed above 
                last = middle - 1; // make last equal to middle minus one 
            } // close the else statement 
            middle = (first + last) / 2; // make the middle equal to the first plus the last divided by two 
        
        } // close the while loop 
        
        System.out.println(number + " was NOT found with " + iterationCount + " iterations"); // print out that the number with the amount of iterations it took to do so. 
        return; // save the value 

    } // close the method 

  public static void shuffle(int[] array){ // create a new method for the shuffling 
  
  for (int i = 0; i < 50; i++){ // create a for loop for when i is less than 50 
    
    int temp; // initialize the variable that is going to act as a dummy variable  
    int randomIndex = (int) (Math.random () * array.length); // declare the variable and get a random value of it
    temp = array[0]; // create a new array with an index at 0 
    array[0] = array[randomIndex]; // pick an index that is random  
    array[randomIndex] = temp; // make that random value that you generate equal to the dummy variable you created 
    
    } // close the the for loop 
  } // close the method 
  public static void linearSearch(int[] array, int number){ // create a method for the linear shearch for the grade the user entered 
    int i = 0; // declare and initialize the varaible  
    for(i = 0; i < array.length; i++){ // create a for loop for when i is less than the length of the array 
      if( array[i] == number){ // create an if statement for when the array at i is equal to the number 
        System.out.println(number + " was found in the list with " + i + " itterations"); // print out the number and how many iterations it took to find it 
      return; // save the value 
      } // close the if statement 
      
    } // close the for loop 
    System.out.println(number + " was not found in the list with " + i + " itterations"); // print out the number and how many iterations it took to find it 
  } // close the method 
} // close the code 
  
