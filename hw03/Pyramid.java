///////
/// CSE 02
// Daniel John
// September 17, 2018
//
import java.util.Scanner;
public class Pyramid {
  
  public static void main(String args[]){
    
double lengthOfPyramid; // Length of the pyramid
double widthOfPyramid; // width of the pyramid
double volumeOfPyramid; // volume of the pyramid
   
Scanner myScanner; // Declare within the main method
myScanner = new Scanner(System.in); // Construct in order to use the object 
    
System.out.print("The square side of the pyramid is (input length): "); // Print out the side of the pyramid
lengthOfPyramid = myScanner.nextDouble(); // length of the pyramid classified into a class
System.out.print("The height of the pyramid is (input height): "); // Print out the height of the pyramid
widthOfPyramid = myScanner.nextDouble(); // width of pyramid classified into a class
    
volumeOfPyramid = (lengthOfPyramid * lengthOfPyramid * widthOfPyramid) / 3; // calculate for the volume of the pyramid by multiplying the length of the pyramid, again with the length of the pyramid and them multiplying that with the width of the pyramid. You then take that value and divide it by 3
    
System.out.println("The volume inside the pyramid is: " + volumeOfPyramid); // Print out the volume inside of the pyramid 
    
    }
  } 