///////
/// CSE 02
// Daniel John
// September 17, 2018
//
// User inputs doubles that represent number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average in cubic miles.
//
import java.util.Scanner;
public class Convert {
  
  public static void main(String args[]){
    
double numAcresOfLand; // Acres of land affected by hurricane precipitation
double rain; // Inches of rain dropped on average

Scanner myScanner; // Declare within the main method
myScanner = new Scanner(System.in); // Construct in order to use the object
double acresTimesRain; // Declare the acre inches of rain
System.out.print("Enter the affected area in acres: "); // Print out the affected area in acres
        
numAcresOfLand = myScanner.nextDouble(); // Number of acres of land classified into a class
System.out.print("Enter the rainfall in the affected area: "); // Print out the rianfall in the affected area 
rain = myScanner.nextDouble(); // Rain classified into a class
acresTimesRain = (numAcresOfLand * rain) / 40550399.587131; // Calculation for acres times rain which you get my multiplying acres and rain and then dividing with the conversion factor
System.out.println(acresTimesRain + " acres of rain"); // Print out acres of rain
    
    }   
}


