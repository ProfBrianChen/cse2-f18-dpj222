////////////
//// CSE 02 
/// Daniel John
/// October 19, 2018
/// 
/// Create nested loops that display a set of pattern of numbers  

import java.util.*; // import class

public class EncryptedX{ // same the class
  
  public static void main(String[] args) { // declaring the method
    
    Scanner myScanner = new Scanner(System.in); // declare a new class of scanner
    int numRows; // declare the variable numRows
    
    System.out.println("Please enter an integer between 1-100: "); // System out print statement to inform the user what to enter
    while (!myScanner.hasNextInt()){ // a while loop in the case that the number entered is not an integer 
      myScanner.next(); // scan the user's input
      System.out.println("Error. Please enter an integer: "); // System out print statement for the error in the case that the user does not enter an integer
    }
    numRows = myScanner.nextInt(); // make the number of rows equal an integer that the system scans 
    
    while (numRows < 1 || numRows > 100){ // create a while statement for the case in which the number entered by the user is outside the interval
      System.out.println("Error. Please input an integer between 1 and 100 only: "); // print out an error statement in the case that the input entered is outside of the interval 
      numRows = myScanner.nextInt(); // make the number of rows equal an integer that the system scans
    }
    for (int i = 0; i < numRows + 1; i++){ // create a for loop for the number of rows which is represented by i
      
      for (int j = 0; j < numRows + 1; j++){ // create a for loop for the number of columns which is represented by j
        
        if  ( i == j || (i) == (numRows - j)  ){ // write an if statement in order to be able to create the spaces needed to make an x with the spaces 
          System.out.print(" "); // the system out print statement that prints out a space at the beginning and end of the firast row, and beginning and end of last row
        } else { // an if the condition does not apply, apply this else statement
          System.out.print("*"); // out print statement printing out all of the starts that are going to make up the message 
          
        } 
        
      } 
      System.out.println(); // print out in new lines after the row is done. 
      
      
    }
  }
}